<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(AppSeeder::class);
        $this->call(ContactSeeder::class);
        $this->call(MenuSeeder::class);
    }
}
