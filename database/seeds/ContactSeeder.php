<?php

use Illuminate\Database\Seeder;
use App\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'name' => 'Mohamamd Rofik',
            'email' => 'ofeck@programmer.net',
            'phone' => '082302060485',
            'address' => 'Pasanggar, Pegantenan, Pamekasan'
        ]);
    }
}
