<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePpdbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ppdb', function (Blueprint $table) {
            $table->increments('id');
            // Form Identitas Siswa
            $table->string('registration_code'); //Kode Registrasi
            $table->string('name'); //Nama Lengkap
            $table->string('address'); //Alamat
            $table->string('birth_place'); //Tempat Lahir
            $table->string('date_of_birth'); //Tanggal Lahir
            $table->string('gender'); //Jenis Kelamin
            $table->string('no_kk')->nullable(); //No KK
            $table->string('nik')->nullable(); //NIK
            $table->string('enroll_in_class')->nullable(); //Masuk di Kelas
            $table->integer('number_of_siblings')->default(0)->nullable(); //Jumlah Saudara Kandung
            $table->integer('number_of_foster_brothers')->default(0)->nullable(); //Jumlah Saudara Angkat
            $table->string('status_in_family')->nullable(); //Status dalam keluarga
            $table->string('photo')->nullable(); //Foto
            $table->string('last_diploma')->nullable(); //Ijazah terakhir
            $table->string('educational_concentration')->nullable(); //Konsetrasi Pendidikan
            $table->string('desire_for_school')->nullable(); //Keinginan untuk mondok
            $table->string('rote_state')->nullable(); //Keadaan hafalan
            $table->string('number_of_memorization')->nullable(); //Jumlah Hafalan
            // Form Keterangan Orang Tua
            $table->string('father_name')->nullable(); //Nama Ayah
            $table->string('father_address')->nullable(); //Alamat ayah
            $table->string('father_birth_place')->nullable(); //Tempat lahir ayah
            $table->string('father_date_of_birth')->nullable(); //Tanggal Lahir ayah
            $table->string('father_occupation')->nullable(); //Pekerjaan Ayah            
            $table->string('mother_name')->nullable(); //Nama Ibu
            $table->string('mother_address')->nullable(); //Alamat Ibu
            $table->string('mother_birth_place')->nullable(); //Tempat lahir Ibu
            $table->string('mother_date_of_birth')->nullable(); //Tanggal Lahir Ibu
            $table->string('mother_occupation')->nullable(); //Pekerjaan Ibu
            // Form Keterangan Wali
            $table->string('guardian_name')->nullable(); //Nama Wali
            $table->string('guardian_address')->nullable(); //Alamat Wali
            $table->string('guardian_birth_place')->nullable(); //Tempat lahir Wali
            $table->string('guardian_date_of_birth')->nullable(); //Tanggal Lahir Wali
            $table->string('guardian_occupation')->nullable(); //Pekerjaan Wali
            $table->string('guardian_income')->nullable(); //Penghasilan wali
            // Form Keterangan Pendidikan
            $table->string('origin_school')->nullable(); //Sekolah Asal
            $table->string('diploma_date')->nullable(); //Tanggal ijazah
            $table->string('diploma_number')->nullable(); //Nomor Ijazah
            $table->string('skhu_date')->nullable(); //Tanggal SKHUN
            $table->string('skhu_number')->nullable(); //Nomor SKHUN
            $table->string('graduation')->nullable(); //Kelulusan
            // Form Tambahan
            $table->integer('tier_id'); //Jenjang
            $table->string('year')->nullable(); //Tahun
            $table->boolean('status')->default(false); //Status
            $table->enum('aproval', ['verify', 'aprove', 'reject'])->default('verify')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppdb');
    }
}
