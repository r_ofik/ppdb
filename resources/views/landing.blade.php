@extends('layouts.app')

@section('content')
    <section class="banner text-center p-0">
        <div class="container-fulid">
            <div id="banner-slider" class="owl-carousel owl-theme">

                @foreach ($sliders as $slider)
                <div class="banner-slide d-flex align-items-center" style="background-image: url(assets/images/slider/{{$slider->image}})">
                    <div class="banner-text w-100">
                        <h1>{{ $slider->title }}</h1>
                        <p class="info-text-slider text-white my-4" ">{{ $slider->description }}</p>
                    </div>
                </div>
                @endforeach
                
            </div>
        </div>
    </section>


    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="page_tital text-center">
                        <h2> {{ $app->column_one }} </h2>
                        <hr class="tital_border">
                    </div>
                </div>
            </div>
            <div class="row mt-4 text-center">

                @foreach ($column_one as $item)
                <div class="col-md-4">
                    <div class="study_block">
                        <div class="icon_block">
                            <img src="{{asset('/assets/images/landing/'.$item->image)}}" alt="img" class="rounded-circle border p-1" width="80" height="80" style="object-fit:cover">
                        </div>
                        <div class="study_info">
                            <h3>{{$item->title}}</h3>
                            <p>{{$item->description}}</p>
                            <a href="{{$item->link}}" class="btn btn-default profile_btn read_more">Lihat</a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>


    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page_tital text-center">
                        <h2> {{ $app->column_two }} </h2>
                        <hr class="tital_border">
                    </div>
                </div>
            </div>

            <div class="row p-t-50">
                @foreach ($column_two as $item)
                <div class="col-sm-6 m-b-20">
                    <div class="row m-b-30">
                        <div class="col-4 col-sm-4">
                            <div class="teachers_pro">
                                <img src="{{asset('/assets/images/landing/'.$item->image)}}" alt="img" class="img-responsive" />
                            </div>
                        </div>
                        <div class="col-8 col-sm-8">
                            <div class="teacher_text">
                                <h3>{{$item->title}}</h3>
                                
                                <p>{{$item->description}}</p>
                                <button class="btn btn-default profile_btn" type="button" onclick='window.location.href="{{$item->link}}"'>
                                    Lihat
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            
        </div>
    </section>

    <section class="bg_grey news_sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page_tital">
                        <h2>Berita Terbaru</h2>
                        <hr class="tital_border m-l-0" />
                    </div>
                </div>
            </div>
            <div class="row m-t-30">
                <div id="latest_blog_carousel" class="owl-carousel owl-theme">
                    @foreach ($posts as $post)
                    <div class="latest_news">
                        <div class="new_blgo">
                            <div class="custom_hover_img">
                            <img src="{{asset('/assets/images/post-cover/'.$post->image)}}" alt="News Post" class="img-responsive" height="200" style="object-fit:cover">
                            </div>
                            <div class="news_date">
                                <h3 class="datesection">{{$post->created_at->format('d')}}</h3>
                                {{$post->created_at->format('M')}}
                            </div>
                        <div class="new_border"></div>
                        </div>
                        <div class="author_name">
                            <h5><a href="/blog/{{$post->id.'-'.urlencode($post->title).'.html'}}">{{$post->title}}</a></h5>
                            {!!str_limit($post->content, 100)!!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection