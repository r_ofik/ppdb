@extends('layouts.landing')

@section('content')
<div class="hero-slide owl-carousel site-blocks-cover">
    @foreach ($sliders as $slider)
    <div class="intro-section" style="background-image: url(assets/images/slider/{{$slider->image}})">
        <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
            <h1>{{ $slider->title }}</h1>
            </div>
        </div>
        </div>
    </div>
    @endforeach
</div>

<div></div>
    
<div class="site-section">
    <div class="container">
    <div class="row mb-5 justify-content-center text-center">
        <div class="col-lg-4 mb-5">
        <h2 class="section-title-underline mb-5">
            <span>{{ $app->column_one }}</span>
        </h2>
        </div>
    </div>
    <div class="row">
        @foreach ($column_one as $item)
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <div class="feature-1 border">
                <div class="icon-wrapper bg-primary text-center">
                    <img src="{{asset('/assets/images/landing/'.$item->image)}}" alt="img" class="rounded-circle" width="80" height="80" style="object-fit:cover;margin-top:-10px;">
                </div>
                <div class="feature-1-content">
                <h2>{{ $item->title }}</h2>
                <p>{{$item->description}}</p>
                <p><a href="{{$item->link}}" class="btn btn-primary px-4 rounded-0">Lihat</a></p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    </div>
</div>

    
<div class="site-section">
    <div class="container">


    <div class="row mb-5 justify-content-center text-center">
        <div class="col-lg-6 mb-5">
        <h2 class="section-title-underline mb-3">
            <span>{{ $app->column_two }}</span>
        </h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="owl-slide-3 owl-carousel">
            @foreach ($column_two as $item)
                <div class="course-1-item">
                <figure class="thumnail">
                    <a href="course-single.html"><img src="assets/land/images/course_1.jpg" alt="Image" class="img-fluid"></a>
                    
                    <div class="category"><h3>{{ $item->title }}</h3></div>  
                </figure>
                <div class="course-1-content pb-4">
                    <h2>{{ $item->description }}</h2>
                    {{-- <div class="rating text-center mb-3">
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    </div> --}}
                    {{-- <p class="desc mb-4">{{ $item->description }}.</p> --}}
                    <p><a href="{{$item->link}}" class="btn btn-primary rounded-0 px-4">Lihat</a></p>
                </div>
                </div>
            @endforeach
            </div>
    
        </div>
    </div>

    
    
    </div>
</div>
    

        
        <div class="news-updates">
          <div class="container">
             
            <div class="row">
              <div class="col-lg-12">
                 <div class="section-heading">
                  <h2 class="text-black">Berita Terbaru</h2>
                  <a href="/blog">Lihat semua berita</a>
                </div>
                <div class="row">

                @foreach ($posts as $key => $post)
                @if ($key == 0 || $key == 1)
                  <div class="col-lg-6">
                @endif
                    <div class="post-entry-big @if ($key != 0) horizontal d-flex mb-4 @endif">
                      <a href="/blog/{{$post->id.'-'.urlencode($post->title).'.html'}}" class="img-link @if ($key != 0) mr-4 @endif"><img src="{{asset('/assets/images/post-cover/'.$post->image)}}" alt="Image" class="img-fluid" @if ($key != 0) width="100px" height="100px" @endif></a>
                      <div class="post-content">
                        <div class="post-meta"> 
                          <a href="/blog/{{$post->id.'-'.urlencode($post->title).'.html'}}">{{$post->created_at->format('D, d-M-Y H:i')}}</a>
                        </div>
                        <h3 class="post-heading"><a href="/blog/{{$post->id.'-'.urlencode($post->title).'.html'}}">{{$post->title}}</a></h3>
                      </div>
                    </div>
                @if ($key == 0)
                  </div>
                @endif
                @endforeach
                   </div>
                
                </div>
            </div>
        
            </div>
          </div>
        </div>


    {{-- 

    <section class="bg_grey news_sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page_tital">
                        <h2>Berita Terbaru</h2>
                        <hr class="tital_border m-l-0" />
                    </div>
                </div>
            </div>
            <div class="row m-t-30">
                <div id="latest_blog_carousel" class="owl-carousel owl-theme">
                    @foreach ($posts as $post)
                    <div class="latest_news">
                        <div class="new_blgo">
                            <div class="custom_hover_img">
                            <img src="{{asset('/assets/images/post-cover/'.$post->image)}}" alt="News Post" class="img-responsive" height="200" style="object-fit:cover">
                            </div>
                            <div class="news_date">
                                <h3 class="datesection">{{$post->created_at->format('d')}}</h3>
                                {{$post->created_at->format('M')}}
                            </div>
                        <div class="new_border"></div>
                        </div>
                        <div class="author_name">
                            <h5><a href="/blog/{{$post->id.'-'.urlencode($post->title).'.html'}}">{{$post->title}}</a></h5>
                            {!!str_limit($post->content, 100)!!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section> --}}
@endsection