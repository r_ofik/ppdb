@extends('layouts.app')

@section('content')
<hr class="mt-0">

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <h2>Hubungi Kami</h2>
                    <hr class="tital_border">
                    <p>Anda dapat menghubungi kami pada kontak di bawah ini.</p>
                </div>
            </div>
        </div>

        <div class="row p-t-50">
            
            <div class="col-sm-4">
                <div class="emial_icon">
                    <a href="javascript:void(0)"><i class="fa fa-envelope-o"></i></a>
                    <h5>Email</h5>
                    <a href="mailto:{{$contact->email}}">{{$contact->email}}</a>
                </div>
            </div>
        
            <div class="col-sm-4">
                <div class="emial_icon">
                    <a href="javascript:void(0)"><i class="fa fa-phone"></i></a>
                    <h5>Telepon</h5>
                    <a href="tel:{{$contact->phone}}">{{$contact->phone}}</a></li>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="emial_icon">
                    <a href="javascript:void(0)"><i class="fa fa-map-marker"></i></a>
                    <h5>Alamat</h5>
                    <p>{{$contact->address}}</p>
                </div>
            </div>

        </div>
        <div class="row">
            
            <div class="col-sm-12 p-t-50">
                <div class="text-center">
                    <h2>Pesan atu masukan</h2>
                    <hr class="tital_border">
                    <p>Kirim pesan atau masukan kepada kami</p>
                </div>
            </div>
        </div>

        <div class="contact_form1 w-100 ml-auto mr-auto m-t-30">
            <form method="post" action="/contact">
                @csrf
                <div class="row">
                    <div class="col-sm-6 ">
                        <input type="text" placeholder="Nama" name="name" class="form-control" required>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Email atau Telepon" name="contact" class="form-control" required="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-sm-offset-1">
                        <div class="form-group">
                            <textarea placeholder="Pesan" name="message" rows="5" class="form-control form_message" required=""></textarea>
                        </div>
                        <button type="submit" class="btn btn-default profile_btn">
                            Kirim
                        </button>
                        @if (Session::has('message'))
                            <span class="alert alert-success float-right"><i class="fa fa-info"></i> {{Session::get('message')}}</span>
                        @endif
                        @if (Session::has('failed'))
                            <span class="alert alert-danger float-right"><i class="fa fa-info"></i> {{Session::get('failed')}}</span>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection