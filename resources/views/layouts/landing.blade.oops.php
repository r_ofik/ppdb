<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $app->name }} - {{ $title }}</title>
    <link rel="icon" href="{{ asset('/assets/images/app/'.$app->icon) }}" type="image/x-icon" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta http-equiv="content-language" content="en-us">
    <meta http-equiv="content-type" content="text/HTML" charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="{{ $app->description }}">
    <meta property="og:description" content="{{ $app->description }}" />
    <meta name="keywords" content="{{ $app->description }}">
    <meta property="og:title" content="{{ $app->title }}" />
    <meta name="author" content="Crescent-Theme">
    <meta name="copyright" content="copyright 2019 Crescent Theme">

    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('assets/land/fonts/icomoon/style.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/land/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/land/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/land/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/land/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/land/css/owl.theme.default.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/land/css/jquery.fancybox.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/land/css/bootstrap-datepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/land/fonts/flaticon/font/flaticon.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/land/css/aos.css') }}">
    <link href="{{ asset('assets/land/css/jquery.mb.YTPlayer.min.css') }}" media="all" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('assets/land/css/style.css') }}">
</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
    
    <div class="site-wrap">

        <div class="site-mobile-menu site-navbar-target">
          <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
              <span class="icon-close2 js-menu-toggle"></span>
            </div>
          </div>
          <div class="site-mobile-menu-body"></div>
        </div>
    
    
        <div class="py-2 bg-light">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-9 d-none d-lg-block">
                <a href="#" class="small mr-3"><span class="icon-question-circle-o mr-2"></span> Ada pertanyaan?</a> 
                <a href="tel:{{ $contact->phone }}" class="small mr-3"><span class="icon-phone2 mr-2"></span> {{ $contact->phone }} </a> 
                <a href="mailto:{{ $contact->email }}" class="small mr-3"><span class="icon-envelope-o mr-2"></span> {{ $contact->email }}</a> 
              </div>
              <div class="col-lg-3 text-right">
                <a href="/register" class="small btn btn-primary px-4 py-2 rounded-0"> Daftar</a>
              </div>
            </div>
          </div>
        </div>
        <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">
    
          <div class="container">
            <div class="d-flex align-items-center">
              <div class="site-logo">
                <a href="index.html" class="d-block">
                    <img src="{{ asset('/assets/images/app/'.$app->logo) }}" alt="Logo" width="100" class="img-fluid">
                </a>
              </div>
              <div class="ml-auto">
                <nav class="site-navigation position-relative text-right" role="navigation">
                  <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                      
                        @foreach ($menus as $menu)
                        <li @if (count($menu['submenu']) != 0 OR count($menu['subpage']) != 0) class="has-children" @endif>
                            <a href="{{ $menu['link'] }}" @if ($menu['newtab']) target="_blank" @endif class="nav-link text-left">{{$menu['name']}}</a>
                            @if (count($menu['submenu']) != 0 OR count($menu['subpage']) != 0)
                            <ul class="dropdown">
                                @foreach ($menu['submenu'] as $submenu)
                                <li class="nav-item"><a href="{{ $submenu->link }}" @if ($submenu->newtab) target="_blank" @endif class="nav-link">{{$submenu->name}}</a></li>    
                                @endforeach
                                @foreach ($menu['subpage'] as $page)
                                    <li class="nav-item"><a href="/page/{{$page->id}}-{{ $page->shortname }}.html" class="nav-link">{{$page->shortname}}</a></li>    
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @endforeach
            
                  </ul>                                                                                                                                                                                                                                                                                          </ul>
                </nav>
    
              </div>
              <div class="ml-auto">
                <div class="social-wrap">
                  {{-- <a href="#"><span class="icon-facebook"></span></a>
                  <a href="#"><span class="icon-twitter"></span></a>
                  <a href="#"><span class="icon-linkedin"></span></a> --}}
    
                  <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
                    class="icon-menu h3"></span></a>
                </div>
              </div>
             
            </div>
          </div>
    
        </header>
        
        @yield('content')
    
        <div class="footer">
          <div class="container">
            <div class="row">
              <div class="col-lg-3">
                <p class="mb-4">
                    <img src="{{ asset('/assets/images/app/'.$app->logo) }}" alt="Logo" class="img-fluid">
                </p>
                <p>{{$app->description}}</p>  
              </div>
              <div class="col-lg-3">
                <h3 class="footer-heading"><span>{{ $app->column_one }}</span></h3>
                <ul class="list-unstyled">
                    @foreach ($column_one as $item)
                    <li><a href="{{$item->link}}">{{$item->title}}</a></li>    
                    @endforeach
                </ul>
              </div>
              <div class="col-lg-3">
                  <h3 class="footer-heading"><span>{{ $app->column_two }}</span></h3>
                  <ul class="list-unstyled">
                        @foreach ($column_two as $item)
                        <li><a href="{{$item->link}}">{{$item->title}}</a></li>    
                        @endforeach
                  </ul>
              </div>
              <div class="col-lg-3">
                  <h3 class="footer-heading"><span>Kontak</span></h3>
                  <p>{{$contact->address}}</p>
                  <ul class="list-unstyled">
                        <li>Telepon : <a href="tel:{{$contact->phone}}">{{$contact->phone}}</a></li>
                        <li>Email : <a href="mailto:{{$contact->email}}">{{$contact->email}}</a></li>
                  </ul>
              </div>
            </div>
    
            <div class="row">
              <div class="col-12">
                <div class="copyright">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Ronggosukowati Studio. 
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        
    
      </div>
      <!-- .site-wrap -->
    
    
      <!-- loader -->
      {{-- <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#51be78"/></svg></div> --}}
    
      <script src="{{ asset('assets/land/js/jquery-3.3.1.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/jquery-migrate-3.0.1.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/jquery-ui.js') }}"></script>
      <script src="{{ asset('assets/land/js/popper.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/owl.carousel.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/jquery.stellar.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/jquery.countdown.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/bootstrap-datepicker.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/jquery.easing.1.3.js') }}"></script>
      <script src="{{ asset('assets/land/js/aos.js') }}"></script>
      <script src="{{ asset('assets/land/js/jquery.fancybox.min.js') }}"></script>
      <script src="{{ asset('assets/land/js/jquery.sticky.js') }}"></script>
      <script src="{{ asset('assets/land/js/jquery.mb.YTPlayer.min.js') }}"></script>
    
    
    
    
      <script src="{{ asset('assets/land/js/main.js') }}"></script>
    

</body>
</html>