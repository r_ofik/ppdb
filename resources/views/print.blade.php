<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulir Pendaftaran Peserta Didik Baru</title>
    <style>
        *{margin: 0;padding: 0}
        .header{text-align: center; margin-top: 10px;}
        hr{border: 0.5px solid black; margin-top: 5px;}
        .container{width: 90%; margin:auto; font-size: 12px;}
        .reg{text-align: center; border:1px solid black; margin: 5px auto; padding: 5px; width: 50%;}
        .reg table{width: 100%;}
        .p-5{padding: 5px;}
        .w-24{width: 24%;}
        .pl-5{padding-left: 5px;}
        .form-data{width: 100%;}
        .alpha{width: 1%; padding-left: 15px;}
        .photo{text-align: center; margin-top: 10px;}
        .photo img{width: 100px; height: 120px; object-fit: cover;}
        .ttd .pendaftar{float: left; text-align: center; font-weight: bold;}
        .ttd .panitia{float: left; margin-left: 470px; text-align: center; font-weight: bold;}
        .footer {clear: both; color: gray; padding-top: 50px; position: relative; bottom: 0;}
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h4>Formulir Pendaftaran Peserta Didik Baru</h4>
            <h2>YAYASAN AL-HIKAM</h2>
            <h4>Pasanggar Pegantenan Pamekasan</h4>
            <h5>Tahun @php echo date('Y') @endphp</h5>
            <hr>
        </div>
    
        <div class="reg">
            <table>
                <thead>
                    <tr>
                        <th>Jenjang Pendidikan</th>
                        <th>: {{ $student->tier->name }}</th>
                    </tr>
                    <tr>
                        <th>Kode Registrasi</th>
                        <th>: {{ $student->registration_code }}</th>
                    </tr>
                </thead>
            </table>
        </div>

        <table class="form-data">
            <tr>
                <td colspan="3" class="p-5"><strong>1. Identitas Peserta Didik</strong></td>
            </tr>
            <tr>
                <td class="alpha">a.</td>
                <td class="pl-5 w-24">Nama Lengkap </td>
                <td>: &nbsp; {{ $student->name }}</td>
            </tr>
            <tr>
                <td class="alpha">b.</td>
                <td class="pl-5 w-24">Alamat </td>
                <td> : &nbsp; {{ $student->address }}</td>
            </tr>
            <tr>
                <td class="alpha">c.</td>
                <td class="pl-5 w-24">Tempat, Tanggal Lahir </td>
                <td> : &nbsp; {{ $student->birth_place }}, @php $date = date_create(explode('T', $student->date_of_birth)[0]); echo date_format($date, 'd - m - Y') @endphp</td>
            </tr>
            <tr>
                <td class="alpha">d.</td>
                <td class="pl-5 w-24">Jenis Kelamin </td>
                <td> : &nbsp; {{ $student->gender }}</td>
            </tr>
            <tr>
                <td class="alpha">e.</td>
                <td class="pl-5 w-24">Status Dalam Keluarga </td>
                <td> : &nbsp; {{ $student->status_in_family }}</td>
            </tr>
            <tr>
                <td class="alpha">f.</td>
                <td class="pl-5 w-24">No KK </td>
                <td> : &nbsp; {{ $student->no_kk }}</td>
            </tr>
            <tr>
                <td class="alpha">g.</td>
                <td class="pl-5 w-24">NIK </td>
                <td> : &nbsp; {{ $student->nik }}</td>
            </tr>
            <tr>
                <td class="alpha">h.</td>
                <td class="pl-5 w-24">Jumlah Saudara Kandung </td>
                <td> : &nbsp; {{ $student->number_of_siblings }}</td>
            </tr>
            <tr>
                <td class="alpha">i.</td>
                <td class="pl-5 w-24">Jumlah Saudara Angkat </td>
                <td> : &nbsp; {{ $student->number_of_foster_brothers }}</td>
            </tr>
            @if ($student->tier->level != 'Awal')
            <tr>
                <td class="alpha">j.</td>
                <td class="pl-5 w-24">Mendftar dikelas </td>
                <td> : &nbsp; {{ $student->enroll_in_class }}</td>
            </tr>
            @if ($student->tier->level != 'Dasar')
            <tr>
                <td class="alpha">k.</td>
                <td class="pl-5 w-24">Ijazah Terkhir </td>
                <td> : &nbsp; {{ $student->last_diploma }}</td>
            </tr>
            @endif
            @if ($student->tier->level == 'Asrama' || $student->tier->level == 'Khusus')
            <tr>
                <td class="alpha">l.</td>
                <td class="pl-5 w-24">Mendftar Di Pondik </td>
                <td> : &nbsp; {{ $student->educational_concentration }}</td>
            </tr>
            <tr>
                <td class="alpha">m.</td>
                <td class="pl-5 w-24">Alasan Mondok </td>
                <td> : &nbsp; {{ $student->desire_for_school }}</td>
            </tr>
            @if ($student->tier->level == 'Khusus')
            <tr>
                <td class="alpha">n.</td>
                <td class="pl-5 w-24">Keadaan Hafalan </td>
                <td> : &nbsp; {{ $student->rote_state }}</td>
            </tr>
            <tr>
                <td class="alpha">o.</td>
                <td class="pl-5 w-24">Jumlah Hafalan </td>
                <td> : &nbsp; {{ $student->number_of_memorization }}</td>
            </tr>
            @endif
            @endif
            @endif
            <tr>
                <td colspan="3" class="p-5"><strong>2. Keterangan Orang Tua</strong></td>
            </tr>
            <tr>
                <td class="alpha"><i>1.</i></td>
                <td class="pl-5 w-24"><i>Ayah</i></td>
                <td></td>
            </tr>
            <tr>
                <td class="alpha"></td>
                <td class="pl-5 w-24">a. &nbsp; Nama</td>
                <td> : &nbsp; {{ $student->father_name }}</td>
            </tr>
            <tr>
                <td class="alpha"></td>
                <td class="pl-5 w-24">b. &nbsp; Alamat</td>
                <td> : &nbsp; {{ $student->father_address }}</td>
            </tr>
            <tr>
                <td class="alpha"></td>
                <td class="pl-5 w-24">c. &nbsp; Tempat, Tanggal Lahir</td>
                <td> : &nbsp; {{ $student->father_birth_place }}, @php $date = date_create(explode('T', $student->father_date_of_birth)[0]); echo date_format($date, 'd - m - Y') @endphp</td>
            </tr>
            <tr>
                <td class="alpha"></td>
                <td class="pl-5 w-24">d. &nbsp; Pekerjaan</td>
                <td> : &nbsp; {{ $student->father_occupation }}</td>
            </tr>
            <tr>
                <td class="alpha"><i>2.</i></td>
                <td class="pl-5 w-24"><i>Ibu</i></td>
                <td></td>
            </tr>
            <tr>
                <td class="alpha"></td>
                <td class="pl-5 w-24">a. &nbsp; Nama</td>
                <td> : &nbsp; {{ $student->mother_name }}</td>
            </tr>
            <tr>
                <td class="alpha"></td>
                <td class="pl-5 w-24">b. &nbsp; Alamat</td>
                <td> : &nbsp; {{ $student->mother_address }}</td>
            </tr>
            <tr>
                <td class="alpha"></td>
                <td class="pl-5 w-24">c. &nbsp; Tempat, Tanggal Lahir</td>
                <td> : &nbsp; {{ $student->mother_birth_place }}, @php $date = date_create(explode('T', $student->mother_date_of_birth)[0]); echo date_format($date, 'd - m - Y') @endphp</td>
            </tr>
            <tr>
                <td class="alpha"></td>
                <td class="pl-5 w-24">d. &nbsp; Pekerjaan</td>
                <td> : &nbsp; {{ $student->mother_occupation }}</td>
            </tr>
            <tr>
                <td colspan="3" class="p-5"><strong>3. Keterangan Wali</strong></td>
            </tr>
            <tr>
                <td class="alpha">a.</td>
                <td class="pl-5 w-24">Nama</td>
                <td> : &nbsp; {{ $student->guardian_name }}</td>
            </tr>
            <tr>
                <td class="alpha">b.</td>
                <td class="pl-5 w-24">Alamat</td>
                <td> : &nbsp; {{ $student->guardian_address }}</td>
            </tr>
            <tr>
                <td class="alpha">c.</td>
                <td class="pl-5 w-24">Tempat, Tanggal Lahir</td>
                <td> : &nbsp; {{ $student->guardian_birth_place }}, @php $date = date_create(explode('T', $student->guardian_date_of_birth)[0]); echo date_format($date, 'd - m - Y') @endphp</td>
            </tr>
            <tr>
                <td class="alpha">d.</td>
                <td class="pl-5 w-24">Pekerjaan</td>
                <td> : &nbsp; {{ $student->guardian_occupation }}</td>
            </tr>
            <tr>
                <td class="alpha">e</td>
                <td class="pl-5 w-24">Penghasilan /bulan</td>
                <td> : &nbsp; {{ $student->guardian_income }}</td>
            </tr>
            @if ($student->tier->level == 'Menengah' || $student->tier->level == 'Atas' || $student->tier->level == 'Asrama')
            <tr>
                <td colspan="3" class="p-5"><strong>4. Keterangan Pendidikan</strong></td>
            </tr>
            <tr>
                <td class="alpha">a.</td>
                <td class="pl-5 w-24">Asal Sekolah</td>
                <td> : &nbsp; {{ $student->origin_school }}</td>
            </tr>
            <tr>
                <td class="alpha">b.</td>
                <td class="pl-5 w-24">Tanggal & Nomor Ijazah</td>
                <td> : &nbsp; {{ $student->diploma_date.' / '.$student->diploma_number }}</td>
            </tr>
            @if ($student->tier->level == 'Menengah' || $student->tier->level == 'Atas')
            <tr>
                <td class="alpha">c.</td>
                <td class="pl-5 w-24">Tanggal & SKHU</td>
                <td> : &nbsp; {{ $student->skhu_date.' / '.$student->skhu_number }}</td>
            </tr>
            <tr>
                <td class="alpha">d.</td>
                <td class="pl-5 w-24">Kelulusan</td>
                <td> : &nbsp; {{ $student->graduation }}</td>
            </tr>
            @endif
            @endif
        </table>

        <div class="photo">
            <img src="assets/images/students/{{ $student->photo }}">
        </div>
        
        <div class="ttd">
            <div class="pendaftar">
                Pendaftar
                <br><br><br><br><br>
                ___________________
            </div>
            <div class="panitia">
                Panitia
                <br><br><br><br><br>
                ___________________
            </div>
        </div>

        <div class="footer">
            _____________________________________ <br>
            Dicetak dengan <i>Aplikasi Pendaftaran Peserta Didik Baru (PPDB)</i> YAYASAN AL-HIKAM. pada : @php echo date('d/m/Y') @endphp
        </div>
    </div>
</body>
</html>