@extends('layouts.app')

@section('content')
<hr class="mt-0">
<div class="container">
    <form action="/blog/search" method="GET">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Pencarian..." required>
            <div class="input-group-append">
                <button class="btn btn-info" type="submit" title="Cari"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</div>
<hr class="mb-0">
<section>
    <div class="container"> 
        <div class="row">
            @foreach ($posts as $post)
            <div class="col-sm-6 col-md-4">
                <div class="event_img">
                    <img src="{{asset('assets/images/post-cover/'.$post->image)}}" alt="img" class="img-responsive" height="200" style="object-fit:cover;">
                </div>
                <div class="event_text">
                    <h4><a href="{{'/blog/'.$post->id.'-'.urlencode($post->title).'.html'}}">{{$post->title}}</a></h4>
                    {!!str_limit($post->content, 100)!!}
                    <span class="event_map">
                        <i class="fa fa-clock-o"></i>{{$post->created_at->format('d-M-Y h:i A')}}
                    </span>
                </div>
            </div>
            @endforeach
        </div>
        <hr>
        {{ $posts->links('vendor.pagination.custom') }}
    </div>
</section>

@endsection