<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$app->name .' - '. $title}}</title>
    <link rel="icon" href="/assets/images/app/{{ $app->icon }}" type="image/x-icon" />
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <div class="container">
        <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="/assets/images/app/{{ $app->logo }}" alt="" width="72" height="72">
        <h2>{{ $app->title }}</h2>
            <p class="lead">{{ $app->description }}.</p>
        </div>
              
              
        <div id="app">
            <router-view></router-view>
            <vue-progress-bar></vue-progress-bar>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">Copyright <i class="fa fa-copyright"></i> 2019 - Ronggosukowati Studio.</p>
        </footer>
    </div>  
    <script src="/js/app.js"></script>
</body>
</html>