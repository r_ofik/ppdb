@extends('layouts.adminlte')

@section('content')
<router-view></router-view>
<vue-progress-bar></vue-progress-bar>
@endsection
