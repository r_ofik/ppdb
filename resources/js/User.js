export default class User {

    constructor(user){
        this.user = user;
    }

    isAdmin(){
        return this.user.type === 'admin';
    }

    isOperator(){
        return this.user.type === 'operator';
    }

    data(){
        return this.user
    }

}