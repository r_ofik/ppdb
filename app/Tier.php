<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tier extends Model
{
    protected $guarded = ['id'];

    public function students(){
        return $this->hasMany('App\Student'); 
    }
}
