<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $this->authorize('isAdmin');
        return Page::latest()->paginate(15);
    }

    public function addPage(Request $request){
        $this->authorize('isAdmin');
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'shortname' => 'required'
        ]);

        if($request->menu !=0){
        $request->merge(['menu' => $request->menu['id']]);
        }

        $store = Page::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Halaman telah disimpan!'] : ['status' => "false", 'message' => 'Halaman gagal disimpan!'];
    }

    public function updatePage($id, Request $request){
        $this->authorize('isAdmin');
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'shortname' => 'required'
        ]);

        if($request->menu !=0){
        $request->merge(['menu' => $request->menu['id']]);
        }else{
            $request->merge(['menu' => 0]);
        }

        $post = Page::find($id);

        $update = $post->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Halaman telah disimpan!'] : ['status' => "false", 'message' => 'Halaman gagal disimpan!'];
    }

    public function show($id){
        $this->authorize('isAdmin');
        return Page::find($id);
    }

    public function searchPage(){
        $this->authorize('isAdmin');
        $q = \Request::get('q');
        return Page::where('title', 'LIKE', "%$q%")->paginate(15);
    }

    public function destroy($id){
        $this->authorize('isAdmin');
        $page = Page::find($id);

        $delete = $page->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
