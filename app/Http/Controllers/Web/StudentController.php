<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;

class StudentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth('api')->user();
        if ($user->type != 'admin') {
            return Student::latest()->with('tier')->where('tier_id', $user->tier_id)->paginate(25);
        }else{
            return Student::latest()->with('tier')->paginate(25);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validations
        $this->validate($request,[
            'name'              => 'required',
            'address'           => 'required',
            'birth_place'       => 'required',
            'date_of_birth'     => 'required',
            'gender'            => 'required',
            'status_in_family'  => 'required',
            'photo'             => 'required',
            'father_name'       => 'required',
            'father_address'    => 'required',
            'father_occupation' => 'required',
            'mother_name'       => 'required',
            'mother_address'    => 'required',
            'mother_occupation' => 'required',
            'guardian_name'       => 'required',
            'guardian_address'    => 'required',
            'guardian_occupation' => 'required'
        ]);

        if ($request->tier['level'] == 'Dasar' || $request->tier['level'] == 'Menengah' || $request->tier['level'] == 'Atas') {
            $this->validate($request, [
                'enroll_in_class' => 'required'
            ]);
        }

        $tier_id = $request->tier['id'];
        //set tier_id
        $request->merge(['tier_id' => $tier_id]);
        
        $request->request->remove('tier');

        //save photo
        $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

        \Image::make($request->photo)->save(public_path('assets/images/students/').$name);
        $request->merge(['photo' => $name]);

        $request->request->add(['year' => date('Y')]);
        $request->request->add(['registration_code' => time()]);
        $request->request->add(['status' => true]);
        $request->request->add(['aproval' => 'aprove']);


        $store = Student::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Peserta Didik telah disimpan!'] : ['status' => "false", 'message' => 'Peserta Didik gagal disimpan!'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Student::find($id)->update(['status'=>true]);

        return Student::where('id', $id)->with('tier')->first();
    }

    public function filter($id)
    {
        return Student::latest()->where('tier_id', $id)->with('tier')->paginate(25);
    }

    public function searchStudent(){
        $q = \Request::get('q');
        $user = auth('api')->user();
        if ($user->type != 'admin') {
            return Student::latest()->where('registration_code', $q)->orWhere('name', 'LIKE', "%$q%")->with('tier')->where('tier_id', $user->tier_id)->paginate(25);
        }else{
            return Student::latest()->where('registration_code', $q)->orWhere('name', 'LIKE', "%$q%")->with('tier')->paginate(25);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //validations
         $this->validate($request,[
            'name'              => 'required',
            'address'           => 'required',
            'birth_place'       => 'required',
            'date_of_birth'     => 'required',
            'gender'            => 'required',
            'status_in_family'  => 'required',
            'photo'             => 'required',
            'father_name'       => 'required',
            'father_address'    => 'required',
            'father_occupation' => 'required',
            'mother_name'       => 'required',
            'mother_address'    => 'required',
            'mother_occupation' => 'required',
            'guardian_name'       => 'required',
            'guardian_address'    => 'required',
            'guardian_occupation' => 'required'
        ]);

        if ($request->tier['level'] == 'Dasar' || $request->tier['level'] == 'Menengah' || $request->tier['level'] == 'Atas') {
            $this->validate($request, [
                'enroll_in_class' => 'required'
            ]);
        }

        $tier_id = $request->tier['id'];
        //set tier_id
        $request->merge(['tier_id' => $tier_id]);
        
        $request->request->remove('tier');

        $student = Student::find($id);

        $currentImage = $student->photo;

        if($request->photo != $currentImage){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('assets/images/students/').$name);
            $request->merge(['photo' => $name]);

            $studentImage = public_path('assets/images/students/').$currentImage;
            if(file_exists($studentImage)){
                @unlink($studentImage);
            }

        }

        $store = $student->update($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Peserta Didik telah disimpan!'] : ['status' => "false", 'message' => 'Peserta Didik gagal disimpan!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);

        $Image = public_path('assets/images/students/').$student->photo;
        if(file_exists($Image)){
            @unlink($Image);
        }

        $delete = $student->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }

    public function aproval($id, $aproval){
        $update = Student::find($id)->update(['aproval' => $aproval]);

        $message = ($aproval == 'aprove') ? 'Siswa telah diterima!' : 'Siswa telah ditolak';

        return ($update) ? ['status' => "true", 'message' => $message] : ['status' => "false", 'message' => 'Data gagal update siswa!'];
    }
}
