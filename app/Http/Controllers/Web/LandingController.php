<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LandingColumn as Landing;
use App\App;

class LandingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $this->authorize('isAdmin');
        $data['column_one'] = [
            'title' => App::first()->column_one,
            'data'  => Landing::where('column', 1)->get()
        ];

        $data['column_two'] = [
            'title' => App::first()->column_two,
            'data'  => Landing::where('column', 2)->get()
        ];

        return $data;
    }

    public function titleOne($title){
        $this->authorize('isAdmin');
        $update = App::find(App::first()->id)->update(['column_one' => $title]);

        return ($update) ? ['status' => "true", 'message' => 'Judul telah disimpan!'] : ['status' => "false", 'message' => 'Judul gagal disimpan!'];
    }

    public function titleTwo($title){

        $update = App::find(App::first()->id)->update(['column_two' => $title]);

        return ($update) ? ['status' => "true", 'message' => 'Judul telah disimpan!'] : ['status' => "false", 'message' => 'Judul gagal disimpan!'];
    }

    public function addContent(Request $request){
        $this->authorize('isAdmin');
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
            'link' => 'required'
        ]);

        if($request->image){
            $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('assets/images/landing/').$name);
            $request->merge(['image' => $name]);

        }

        $store = Landing::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Konten telah disimpan!'] : ['status' => "false", 'message' => 'Konten gagal disimpan!'];
    }

    public function show($id){
        $this->authorize('isAdmin');
        return Landing::find($id);
    }

    public function update($id, Request $request){
        $this->authorize('isAdmin');
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
            'link' => 'required'
        ]);

        $landing = Landing::find($id);

        $currentImage = $landing->image;

        if($request->image != $currentImage){
            $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('assets/images/landing/').$name);
            $request->merge(['image' => $name]);

            $landingImage = public_path('assets/images/landing/').$currentImage;
            if(file_exists($landingImage)){
                @unlink($landingImage);
            }

        }

        $update = $landing->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Konten telah disimpan!'] : ['status' => "false", 'message' => 'Konten gagal disimpan!'];
    }

    public function destroy($id){
        $this->authorize('isAdmin');
        $landing = Landing::find($id);

        $landingImage = public_path('assets/images/landing/').$landing->image;
        if(file_exists($landingImage)){
            @unlink($landingImage);
        }

        $delete = $landing->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
