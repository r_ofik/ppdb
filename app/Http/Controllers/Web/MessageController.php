<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $this->authorize('isAdmin');
        return Message::latest()->paginate(15);
    }

    public function show($id){
        $this->authorize('isAdmin');
        return Message::find($id);
    }

    public function updateStatus($id){
        $this->authorize('isAdmin');
        $message = Message::find($id);

        $update =  $message->update([
            'status' => true
        ]);

        return ($update) ? ['status' => "true", 'message' => 'Status telah diubah!'] : ['status' => "false", 'message' => 'Status gagal diubah!'];
    }

    public function searchMessage(){
        $this->authorize('isAdmin');
        $q = \Request::get('q');
        return Message::where('name', 'LIKE', "%$q%")->orWhere('contact', 'LIKE', "%$q%")->paginate(15);

    }

    public function destroy($id){
        $this->authorize('isAdmin');
        $message = Message::find($id);

        $delete = $message->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }

}
