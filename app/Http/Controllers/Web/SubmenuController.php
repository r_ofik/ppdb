<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Submenu;
use App\Menu;

class SubmenuController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $this->authorize('isAdmin');
        $submenu = Submenu::all();

        $data = [];
        foreach ($submenu as $menu) {
            $data[] = [
                'id'    => $menu->id,
                'menu'  => Menu::where('id', $menu->menu)->first(),
                'name'  => $menu->name,
                'newtab' => $menu->newtab,
                'link'  => $menu->link
            ];
        }

        return $data;
    }

    public function create(Request $request){
        $this->authorize('isAdmin');
        $this->validate($request,[
            'menu' => 'required',
            'name' => 'required',
            'link' => 'required'
        ]);

        $request->merge(['menu' => $request->menu['id']]);

        $store = Submenu::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Submenu telah disimpan!'] : ['status' => "false", 'message' => 'Submenu gagal disimpan!'];
    
    }

    public function update($id, Request $request){
        $this->authorize('isAdmin');
        $this->validate($request,[
            'menu' => 'required',
            'name' => 'required',
            'link' => 'required'
        ]);

        $menu = Submenu::find($id);

        $request->merge(['menu' => $request->menu['id']]);

        $update = $menu->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Submenu telah disimpan!'] : ['status' => "false", 'message' => 'Submenu gagal disimpan!'];
    }

    public function destroy($id){
        $this->authorize('isAdmin');
        $menu = Submenu::find($id);

        $delete = $menu->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
