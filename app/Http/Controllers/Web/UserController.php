<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Tier;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $user = auth('api')->user();
        $users = User::where('id', '!=', $user->id)->get();
        
        $data = [];
        if (count($users) > 0) {
            foreach ($users as $usr) {
                $data[] = [
                    'id'    => $usr->id,
                    'name'  => $usr->name,
                    'email' => $usr->email,
                    'type'  => ($usr->type == 'admin') ? 'Admin' : 'Operator',
                    'tier' => (Tier::find($usr->tier_id)) ? Tier::find($usr->tier_id)->name : '',
                    'photo' => $usr->photo
                ];
            }
        }
        return $data;
    }

    public function show($id){
        return User::find($id);
    }

    public function update($id, Request $request){
        $this->authorize('isAdmin');
        $user = User::find($id);

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'type'  => 'required',
            'photo' => 'required'
        ]);

        if ($request->type == 'operator') {
            $this->validate($request,[
                'tier' => 'required'
            ]);
            $request->merge(['tier_id'=>$request->tier['id']]);
            unset($request['tier']);
        }

        $currentPhoto = $user->photo;

        if($request->photo != $currentPhoto){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('assets/images/app/').$name);
            $request->merge(['photo' => $name]);

            $userPhoto = public_path('assets/images/app/').$currentPhoto;
            if(file_exists($userPhoto)){
                @unlink($userPhoto);
            }

        }


        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }

        $update = $user->update($request->all());
        return ($update) ? ['status' => "true", 'message' => 'Data telah disimpan!'] : ['status' => "false", 'message' => 'Data gagal disimpan!'];

    }

    public function store(Request $request){
        $this->authorize('isAdmin');
        $user = auth('api')->user();

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|required|min:6',
            'type'  => 'required',
            'image' => 'required'
        ]);

        if ($request->type == 'operator') {
            $this->validate($request,[
                'tier' => 'required'
            ]);
        }

        $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

        \Image::make($request->image)->save(public_path('assets/images/app/').$name);
        $request->merge(['image' => $name]);

        $store = User::create([
            'name'  => $request->name,
            'type'  => $request->type,
            'tier_id'  => $request->tier['id'],
            'email' => $request->email,
            'password'  => bcrypt($request->password),
            'photo' => $request->image
        ]);

        return ($store) ? ['status' => "true", 'message' => 'Pengguna berhasil ditambahkan!'] : ['status' => "false", 'message' => 'Gagal Menambahkan Pengguna!'];
    }

    public function destroy($id){
        $this->authorize('isAdmin');
        $user = User::find($id);

        $userImage = public_path('assets/images/app/').$user->photo;
        if(file_exists($userImage)){
            @unlink($userImage);
        }

        $delete = $user->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }

    public function userInfo(){
        $user = auth('api')->user();

        return $user;
    }

    public function updateProfile(Request $request){
        $user = auth('api')->user();


        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|required|min:6'
        ]);


        $currentPhoto = $user->photo;


        if($request->photo != $currentPhoto){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('assets/images/app/').$name);
            $request->merge(['photo' => $name]);

            $userPhoto = public_path('assets/images/app/').$currentPhoto;
            if(file_exists($userPhoto)){
                @unlink($userPhoto);
            }

        }


        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }

        $user->update($request->all());
        return ['status' => "true", 'message' => 'Data telah diubah!'];
    }
}
