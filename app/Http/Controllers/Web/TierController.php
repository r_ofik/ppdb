<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tier;

class TierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        return Tier::all();
    }

    public function show($id){
        return Tier::find($id);
    }

    public function createTier(Request $request){
        $this->authorize('isAdmin');
        $this->validate($request,[
            'name' => 'required'
        ]);

        $store = Tier::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Jenjang telah dibuat!'] : ['status' => "false", 'message' => 'Jenjang gagal dibuat!'];
    
    }

    public function updateTier($id, Request $request){
        $this->authorize('isAdmin');
        $this->validate($request,[
            'name' => 'required'
        ]);

        $tier = Tier::find($id);

        $update = $tier->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Jenjang telah disimpan!'] : ['status' => "false", 'message' => 'Jenjang gagal disimpan!'];
    }

    public function destroy($id){
        $this->authorize('isAdmin');
        $tier = Tier::find($id);

        $delete = $tier->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
