<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\App;
use App\Tier;
use App\Student;
use PDF;
class RegisterController extends Controller
{
    public function register(){
        $data['app'] = App::first();
        $data['title'] = 'Register';

        return view('register', $data);
    }

    public function gettiers(){
        return Tier::all();
    }

    public function store(Request $request){
        //validations
        $this->validate($request,[
            'name'              => 'required',
            'address'           => 'required',
            'birth_place'       => 'required',
            'date_of_birth'     => 'required',
            'gender'            => 'required',
            'status_in_family'  => 'required',
            'photo'             => 'required',
            'father_name'       => 'required',
            'father_address'    => 'required',
            'father_occupation' => 'required',
            'mother_name'       => 'required',
            'mother_address'    => 'required',
            'mother_occupation' => 'required',
            'guardian_name'       => 'required',
            'guardian_address'    => 'required',
            'guardian_occupation' => 'required'
        ]);

        if ($request->tier['level'] == 'Dasar' || $request->tier['level'] == 'Menengah' || $request->tier['level'] == 'Atas') {
            $this->validate($request, [
                'enroll_in_class' => 'required'
            ]);
        }

        $tier_id = $request->tier['id'];
        //set tier_id
        $request->merge(['tier_id' => $tier_id]);
        
        $request->request->remove('tier');

        //save photo
        $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

        \Image::make($request->photo)->save(public_path('assets/images/students/').$name);
        $request->merge(['photo' => $name]);

        $request->request->add(['year' => date('Y')]);
        $request->request->add(['registration_code' => time()]);
        $request->request->add(['status' => false]);
        $request->request->add(['aproval' => 'verify']);


        $store = Student::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Registrasi Berhasil!', 'data' => $store->id] : ['status' => "false", 'message' => 'Registrasi Gagal!'];
    }

    public function registered($id){
        return Student::where('id', $id)->with('tier')->first();
    }

    public function print($id){
        $data['student'] = Student::where('id', $id)->with('tier')->first();
        $data['app'] = App::first();
        $form = PDF::loadview('print', $data);
        return $form->stream('Formulir Pendaftaran Peserta Didik Baru');
    }
}
