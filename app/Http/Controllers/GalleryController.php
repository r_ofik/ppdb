<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\App;
use App\Contact;
use App\Menu;
use App\Page;
use App\LandingColumn as Landing;
use App\Gallery;
use App\Submenu;

class GalleryController extends Controller
{
    public function index(){
        $data['app'] = App::first();
        $data['contact'] = Contact::first();
        $data['title'] = 'Galery';

        $datamenu = Menu::get();
        
        foreach ($datamenu as $menu) {
            $data['menus'][] = [
                'name'      => $menu->name,
                'link'      => $menu->link,
                'newtab'    => $menu->newtab,
                'submenu'   => Submenu::where('menu', $menu->id)->get(),
                'subpage'   => Page::where('menu', $menu->id)->get()
            ];
        }

        $data['column_one'] = Landing::where('column', 1)->get();
        $data['column_two'] = Landing::where('column', 2)->get();
        $data['galleries'] = Gallery::all();

        return view('gallery', $data);
    }
}
