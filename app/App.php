<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    protected $table = 'app';

    protected $fillable = [
        'name', 'title', 'icon', 'logo', 'description', 'column_one', 'column_two'
    ];
}
