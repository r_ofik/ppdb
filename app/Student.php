<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'ppdb';
    protected $guarded = ['id'];

    public function tier(){
        return $this->belongsTo('App\Tier');
    }
}
