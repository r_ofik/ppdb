<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/profile', 'Web\UserController@userInfo');
Route::put('/profile', 'Web\UserController@updateProfile');

Route::get('/users', 'Web\UserController@index');
Route::post('/add-user', 'Web\UserController@store');
Route::delete('/user/{id}', 'Web\UserController@destroy');
Route::get('/user/{id}', 'Web\UserController@show');
Route::put('/user/{id}', 'Web\UserController@update');

Route::get('/appInfo', 'Web\AppController@appInfo');
Route::put('/app', 'Web\AppController@updateApp');
Route::get('/appContact', 'Web\AppController@appContact');
Route::put('/appContact', 'Web\AppController@updateContact');

Route::get('/posts', 'Web\PostController@index');
Route::post('/savePost', 'Web\PostController@addPost');
Route::put('/savePost/{id}', 'Web\PostController@updatePost');
Route::get('/post/{id}', 'Web\PostController@show');
Route::delete('/post/{id}', 'Web\PostController@destroy');
Route::get('/searchPost', 'Web\PostController@searchPost');

Route::get('/pages', 'Web\PageController@index');
Route::post('/savePage', 'Web\PageController@addPage');
Route::put('/savePage/{id}', 'Web\PageController@updatePage');
Route::get('/page/{id}', 'Web\PageController@show');
Route::delete('/page/{id}', 'Web\PageController@destroy');
Route::get('/searchPage', 'Web\PageController@searchPage');

Route::get('/getmenu', 'Web\MenuController@index');
Route::get('/getmenu/{id}', 'Web\MenuController@show');
Route::post('/menu', 'Web\MenuController@createMenu');
Route::delete('/menu/{id}', 'Web\MenuController@destroy');
Route::put('/menu/{id}', 'Web\MenuController@updateMenu');

Route::get('/tiers', 'Web\TierController@index');
Route::get('/tier/{id}', 'Web\TierController@show');
Route::post('/tier', 'Web\TierController@createTier');
Route::delete('/tier/{id}', 'Web\TierController@destroy');
Route::put('/tier/{id}', 'Web\TierController@updateTier');

Route::get('/slider', 'Web\SliderController@index');
Route::get('/slider/{id}', 'Web\SliderController@show');
Route::post('/slider', 'Web\SliderController@createSlider');
Route::delete('/slider/{id}', 'Web\SliderController@destroy');
Route::put('/slider/{id}', 'Web\SliderController@updateSlider');

Route::get('/galery', 'Web\GaleryController@index');
Route::get('/galery/{id}', 'Web\GaleryController@show');
Route::post('/galery', 'Web\GaleryController@createGalery');
Route::delete('/galery/{id}', 'Web\GaleryController@destroy');
Route::put('/galery/{id}', 'Web\GaleryController@updateGalery');

Route::get('/message', 'Web\MessageController@index');
Route::get('/message/{id}', 'Web\MessageController@show');
Route::get('/searchMessage', 'Web\MessageController@searchMessage');
Route::put('/message/{id}', 'Web\MessageController@updateStatus');
Route::delete('/message/{id}', 'Web\MessageController@destroy');

Route::get('/submenu', 'Web\SubmenuController@index');
Route::get('/submenu/{id}', 'Web\SubmenuController@show');
Route::post('/submenu', 'Web\SubmenuController@create');
Route::delete('/submenu/{id}', 'Web\SubmenuController@destroy');
Route::put('/submenu/{id}', 'Web\SubmenuController@update');

Route::get('/landing', 'Web\LandingController@index');
Route::get('/landing/{id}', 'Web\LandingController@show');
Route::put('/landing/{id}', 'Web\LandingController@update');
Route::post('/landing/titleOne/{title}', 'Web\LandingController@titleOne');
Route::post('/landing/titleTwo/{title}', 'Web\LandingController@titleTwo');
Route::post('/landing/addContent', 'Web\LandingController@addContent');
Route::delete('/landing/{id}', 'Web\LandingController@destroy');

Route::apiResource('/student', 'Web\StudentController');
Route::put('/aproval/{id}/{aproval}', 'Web\StudentController@aproval');
Route::get('/searchStudent', 'Web\StudentController@searchStudent');
Route::get('/filter/{id}', 'Web\StudentController@filter');