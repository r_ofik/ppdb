<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingController@index')->name('landing');

// Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Routing for admin
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/blog/create-post', 'HomeController@index')->name('home');
Route::get('/blog/posts', 'HomeController@index')->name('home');
Route::get('/sliders', 'HomeController@index')->name('home');
Route::get('/add-slider', 'HomeController@index')->name('home');
Route::get('/edit-slider', 'HomeController@index')->name('home');
Route::get('/gallery', 'HomeController@index')->name('home');
Route::get('/settings/app', 'HomeController@index')->name('home');
Route::get('/settings/menu', 'HomeController@index')->name('home');
Route::get('/settings/landing', 'HomeController@index')->name('home');
Route::get('/settings/users', 'HomeController@index')->name('home');
Route::get('/settings/add-user', 'HomeController@index')->name('home');
Route::get('/settings/edit-user', 'HomeController@index')->name('home');
Route::get('/settings/add-content-one', 'HomeController@index')->name('home');
Route::get('/settings/add-content-two', 'HomeController@index')->name('home');
Route::get('/settings/edit-content', 'HomeController@index')->name('home');
Route::get('/settings/tier', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@index')->name('home');
Route::get('/blog/edit-post', 'HomeController@index')->name('home');
Route::get('/create-page', 'HomeController@index')->name('home');
Route::get('/pages', 'HomeController@index')->name('home');
Route::get('/edit-page', 'HomeController@index')->name('home');
Route::get('/create-gallery', 'HomeController@index')->name('home');
Route::get('/edit-gallery', 'HomeController@index')->name('home');
Route::get('/messages', 'HomeController@index')->name('home');
Route::get('/show-message', 'HomeController@index')->name('home');
Route::get('/students', 'HomeController@index')->name('home');
Route::get('/edit-student', 'HomeController@index')->name('home');
Route::get('/show-student', 'HomeController@index')->name('home');
Route::get('/add-student', 'HomeController@index')->name('home');


//Routing Landing
Route::get('/galleries', 'GalleryController@index');
Route::get('/contact', 'BlogController@contact');
Route::post('/contact', 'BlogController@message');
Route::get('/blog', 'BlogController@index');
Route::get('/blog/search', 'BlogController@search');
Route::get('/blog/{id}-{path}', 'BlogController@show')->where('path', '([A-z\d\-\/_.+]+?)');
Route::get('/page/{id}-{path}', 'BlogController@page')->where('path', '([A-z\d\-\/_.+]+?)');
Route::get('/tiers', 'LandingController@tiers');
Route::get('/register', 'RegisterController@register');
Route::post('/register', 'RegisterController@store');
Route::get('/registered', 'RegisterController@register');
Route::get('/registered/{id}', 'RegisterController@registered');
Route::get('/print/{id}', 'RegisterController@print');
Route::get('/tiers', 'RegisterController@gettiers');
// Route::get('/{path}', 'HomeController@index')->where('path', '([A-z\d\-\/_.]+?)');
Route::get('/forbiden', function(){
    return view('errors/403');
});